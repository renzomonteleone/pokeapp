import { defineStore } from 'pinia';
import { ref } from 'vue'
import { useRouter } from 'vue-router';
import axios from 'axios'

export const useFilterStore = defineStore("filter", () => {
    const router = useRouter();

    const results = ref([])
    const pokemon: any = ref({})
    const next = ref("")
    const previous = ref("")
    const count = ref(0)
    const actualPage = ref(1)
    const nameFilter = ref("")
    const loading = ref(false)

    const fetchPage = (page: number) => {
        results.value = [];
        loading.value = true;
        axios
            .get(`https://pokeapi.co/api/v2/pokemon/?limit=10&offset=${(page * 10) - 10}`)
            .then((response) => {
                actualPage.value = page;
                results.value = response.data.results;
                count.value = response.data.count;
                next.value = response.data.next;
                previous.value = response.data.previous;
                loading.value = false;
            }).catch(() => {
                alert("There was an error fetching the API")
                loading.value = false
            })
    }

    const searchbyName = () => {
        loading.value = true;
        axios
            .get(`https://pokeapi.co/api/v2/pokemon/${(nameFilter.value).toLowerCase()}`)
            .then((response) => {
                fetchPage(actualPage.value)
                pokemon.value = response.data
                loading.value = false
                nameFilter.value = ""
                router.push('/details')
            }).catch(() => {
                alert("No results")
                loading.value = false
            })
    }

    return { results, nameFilter, loading, actualPage, previous, next, pokemon, fetchPage, searchbyName }
})
