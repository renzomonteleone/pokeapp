import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.test.pokeapp',
  appName: 'pokeApp',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
